# usb/tf卡启动说明 / boot from usb or tf card
制作完成后的镜像请烧录入u盘/移动硬盘/tf卡中启动，请先确认机器支持从usb或者tf卡启动，启动后默认自动刷入emmc，完成后自动关机。刷机日志*install_log.txt*可在刷机后*BOOT*分区中找到。

After patching the image, please burn it into the flash-disk / portable hard drive / tf cards. Make sure your box can boot from usb or tf. The OS will install to the built-in emmc by default, and it will auto poweroff after installation. You can find install log *install_log.txt* in *BOOT* partition. 

## 启动/刷机控制 / boot/install control
以下操作均在*BOOT*分区进行，该分区为FAT32分区，Windows下可读写。
- 创建空文件*changemmc.txt*可以将系统安装至mmc0总线上的emmc中，这取决于机型，例如R3300-M，大部分机器无需改动
- 创建空文件*replaceuboot.txt*可以替换uboot，这针对部分p212/p211兼容机原厂uboot未初始化网卡导致Armbian无法使用网口，如部分ty1208z
- 创建空文件*notautoinstall.txt*可以禁用自动刷机功能，启动后将不会自动将系统安装至内部emmc
- 创建文件*macaddr.txt*并填入mac地址可以设为mac地址，如aa:bb:cc:dd:ee:ff [支持mac和ethaddr两种u-boot环境变量]

these operation should be done in *BOOT* partition. This is a FAT32 partition, which can be seen in Windows.
- add a new empty file *changemmc.txt* and then the os will be install to the emmc in mmc0 bus, this depends on your meachine, for example, R3300-M. for most boxes, no need to use this feature
- add a new empty file *replaceuboot.txt* and then the uboot will be replace. This is useful for some boxes whose uboot doesn't initial the ethernet phy, for example, a amount of ty1208z
- add a new empty file *notautoinstall.txt* and then the os will not install itself into built-in emmc.

## uboot命令提示
若需要ttl输入uboot命令以从u盘/tf卡启动，可以参考：

### usb启动 / boot from usb
> usb start;

> if fatload usb 0 1020000 s905_autoscript; then autoscr 1020000; fi; if fatload usb 1 1020000 s905_autoscript; then autoscr 1020000; fi; if fatload usb 2 1020000 s905_autoscript; then autoscr 1020000; fi; if fatload usb 3 1020000 s905_autoscript; then autoscr 1020000; fi;

### tf卡启动 / boot from tf card
> mmcinfo;

>if fatload mmc 0 1020000 s905_autoscript; then autoscr 1020000; fi;

若当前系统为安卓，建议先不插入u盘/tf卡，以免进uboot识别进入安卓系统损坏镜像，先确认进入uboot环境再插入u盘/tf卡，然后输入命令。

if current os is Android, enter the uboot environment first and then insert your usb disk or tf is better, in order to prevent Android destory the Armbian files.

启动过一次Armian后，后续u盘/tf卡启动优先级大于内置emmc，再次刷机无需进入uboot输入命令。

The boot order of usb or tf has benn greater then built-in emmc before you boot an Armbian, if you install the Armbian again, no need to enter uboot and give those commands.
