# aml-linux-image-patch

#### Description
Scripts to modify image for s905 to optimize something
修改时区、换清华源、放入自动安装脚本、替换dtb等，可以修改普通armbian镜像，生成自动安装的u盘镜像。支持armbian5.xx到20.07，后面的Armbian启动分区脚本似乎有改动，这边需要做相应改动。

#### Software Architecture
script.sh 记得打开修改你的镜像挂载目录，检查权限，需要root运行，默认设置是给N1的，适合ubuntu，需要其他功能请自行修改（给debian的也须修改最后几行换源的命令，其他组装镜像经过修改也可以使用，install.sh脚本已适配centos）

target 目录是一些待替换的文件，clone下来，如果是脚本记得检查权限！其中uboot压缩包用于天邑ty1208z，用于替换部分机器无网卡初始化功能的uboot。

