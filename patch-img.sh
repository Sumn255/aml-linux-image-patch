#!/bin/bash

[ "$EUID" != "0" ] && echo "please run as root" && exit 1

DIR=$(pwd)
BOOTDIR="/run/media/sumn/BOOT/" # modify your mount path
ROOTFSDIR="/run/media/sumn/ROOTFS/" # modify your mount path

cd $BOOTDIR || exit 1
#cp -f ${DIR}/target/bonuscloud-ty1208.dtb ./dtb/ # for ty1208 box or other p211 board
#sed -i '/^dtb_name=/cdtb_name=/dtb/bonuscloud-ty1208.dtb' ./uEnv.ini #for ty1208 box or other p211 board
cp -f ${DIR}/target/bonuscloud-phicomn-n1.dtb ./dtb/ #for phicomn n1
sed -i '/^dtb_name=/cdtb_name=/dtb/bonuscloud-phicomn-n1.dtb' ./uEnv.ini #for phicomn n1
sync

cd $ROOTFSDIR || exit 1
#cp -f ${DIR}/target/preinstall.sh ./root
cp -f ${DIR}/target/install.sh ./root
#cp -f ${DIR}/target/uboot-ty1208.img.xz ./root/uboot-ty1208.img.xz #for ty-1208 box only
#cp -f ${DIR}/target/sources.list ./etc/apt/sources.list # change mirror ,or use below methods
cp -f ${DIR}/target/rc.local ./etc/rc.local
[ ! -f ./root/fstab ] && cp -f ${DIR}/target/fstab ./root/fstab
rm -f ./root/install-2018.sh
rm -f ./etc/systemd/system/getty.target.wants/serial-getty@ttyS0.service
ln -sf /usr/share/zoneinfo/Asia/Shanghai ./etc/localtime
sed -i '/^#NTP=/cNTP=time1.aliyun.com 2001:470:0:50::2' ./etc/systemd/timesyncd.conf
echo "Asia/Shanghai" > ./etc/timezone
cat > ./root/.bash_aliases << EOF
alias df='df -Th'
alias free='free -h'
alias ls='ls -hF --color=auto'
alias ll='ls -AlhF --color=auto'
EOF
#echo "blacklist hci_uart" >>./etc/modprobe.d/blacklist.conf #to disable bluetooth
sed -i 's/apt.armbian.com/mirrors.tuna.tsinghua.edu.cn\/armbian/' ./etc/apt/sources.list.d/armbian.list
sed -i 's/http/https/' ./etc/apt/sources.list.d/armbian.list
#echo "deb http://security.debian.org/debian-security stretch/updates main contrib non-free" >> ./etc/apt/sources.list
sed -i 's/ports.ubuntu.com/mirrors.tuna.tsinghua.edu.cn\/ubuntu-ports/' ./etc/apt/sources.list # for ubuntu
sed -i 's/mirrors.debian.com/mirrors.tuna.tsinghua.edu.cn\/debian/' ./etc/apt/sources.list # for debian
sed -i 's/http/https/' ./etc/apt/sources.list
sync
